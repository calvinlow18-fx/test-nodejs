#!/bin/sh

SERVICE_NAME=testing_node

docker stop $SERVICE_NAME || true && docker rm $SERVICE_NAME || true
docker build -t $SERVICE_NAME .
docker run \
    --name $SERVICE_NAME \
    $SERVICE_NAME
docker logs --follow $SERVICE_NAME